/* eslint-disable brace-style */
/* eslint-disable operator-linebreak */

// To calculate the blast, we have to calculate the (to be affected) nearby cells
const findNeighbour = (i, j, type) => {
  let neighbour;
  switch (type) {
    // Neighbour of corner cells
    case 'corner':
      if (i === 0 && j === 0) neighbour = [`${i}${j + 1}`, `${i + 1}${j}`];
      else if (i === 0 && j === 9) neighbour = [`${i}${j - 1}`, `${i + 1}${j}`];
      else if (i === 9 && j === 0) neighbour = [`${i - 1}${j}`, `${i}${j + 1}`];
      else neighbour = [`${i - 1}${j}`, `${i}${j - 1}`];
      return neighbour;
    // Neighbour of edge cells
    case 'edge':
      if (i === 0) neighbour = [`${i}${j - 1}`, `${i + 1}${j}`, `${i}${j + 1}`];
      else if (j === 0) {
        neighbour = [`${i - 1}${j}`, `${i}${j + 1}`, `${i + 1}${j}`];
      } else if (j === 9) {
        neighbour = [`${i - 1}${j}`, `${i}${j - 1}`, `${i + 1}${j}`];
      } else {
        neighbour = [`${i}${j - 1}`, `${i - 1}${j}`, `${i}${j + 1}`];
      }
      return neighbour;
    // Neighbour of other cells
    default:
      neighbour = [
        `${i - 1}${j}`,
        `${i}${j + 1}`,
        `${i + 1}${j}`,
        `${i}${j - 1}`,
      ];
      return neighbour;
  }
};

const createReactionGround = () => {
  const reactionGround = [];
  for (let i = 0; i < 10; i += 1) {
    for (let j = 0; j < 10; j += 1) {
      // Set the corner piece properties
      if (
        (i === 0 && j === 0) ||
        (i === 0 && j === 9) ||
        (i === 9 && j === 0) ||
        (i === 9 && j === 9)
      ) {
        reactionGround.push({
          id: `${i}${j}`,
          maxLimit: 1,
          numberOfNodes: 0,
          owner: '',
          neighbour: findNeighbour(i, j, 'corner'),
        });
      }
      // Set the edge piece properties
      else if (i === 0 || i === 9 || j === 0 || j === 9) {
        reactionGround.push({
          id: `${i}${j}`,
          maxLimit: 2,
          numberOfNodes: 0,
          owner: '',
          neighbour: findNeighbour(i, j, 'edge'),
        });
      }
      // Set the remaining piece properties
      else {
        reactionGround.push({
          id: `${i}${j}`,
          maxLimit: 3,
          numberOfNodes: 0,
          owner: '',
          neighbour: findNeighbour(i, j, 'rest'),
        });
      }
    }
  }
  return JSON.parse(JSON.stringify(reactionGround));
};

export default createReactionGround;
