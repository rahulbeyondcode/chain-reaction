import React, { useState, useEffect } from 'react';

import GameRules from './GameRules';
import createReactionGround from '../Helpers/CreateGround';

const PlayZone = () => {
  // const PlayZone = (props) => {
  const [playerSquares, setPlayerSquares] = useState([]);
  // const [currentPlayer, setCurrentPlayer] = useState('');
  const [allShellsToExplode, setShellsToExplode] = useState([]);

  useEffect(() => {
    setPlayerSquares(createReactionGround());
  }, []);

  const explodeShell = (id, allPlayerSquares) => {
    const allSquares = [...JSON.parse(JSON.stringify(allPlayerSquares))];
    const shellsToExplode = [];

    allSquares.forEach((item, index) => {
      if (item.id === id) {
        allSquares[index].numberOfNodes = 0;
        allSquares[index].neighbour.forEach((neighbour) => {
          if (
            allSquares[Number(neighbour)].numberOfNodes ===
            allSquares[Number(neighbour)].maxLimit
          ) {
            shellsToExplode.push(neighbour);
          } else {
            allSquares[Number(neighbour)].numberOfNodes += 1;
          }
        });
        console.log('shellsToExplode: ', shellsToExplode);
      }
    });
    setPlayerSquares(allSquares);
    setShellsToExplode(shellsToExplode);
  };

  const boxClicked = (clickItem) => {
    const allSquares = [...JSON.parse(JSON.stringify(playerSquares))];
    allSquares.forEach((item, index) => {
      if (item.id === clickItem.id) {
        if (allSquares[index].numberOfNodes < allSquares[index].maxLimit) {
          allSquares[index].numberOfNodes += 1;
          setPlayerSquares(allSquares);
        } else {
          explodeShell(item.id, allSquares);
        }
      }
    });
  };

  useEffect(() => {
    if (allShellsToExplode.length) {
      allShellsToExplode.forEach((shell) => {
        console.log('shell: ', shell);
        explodeShell(shell, playerSquares);
      });
      // setShellsToExplode([]);
    }
  }, [allShellsToExplode]);

  // console.log('playerSquares: ', playerSquares[0]);

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <GameRules />
      <div className="playground-holder">
        <div className="playground">
          {playerSquares.map((item) => (
            <div
              unselectable="on"
              className="player-squares"
              role="button"
              onKeyDown={(e) => {
                if (e.which === 13) {
                  boxClicked(item);
                }
              }}
              onClick={() => boxClicked(item)}
              aria-label="play-box-click"
              tabIndex={-1}
              key={item.id}
            >
              {item.owner}
              {item.numberOfNodes}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default PlayZone;
