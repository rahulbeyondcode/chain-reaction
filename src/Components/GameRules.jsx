import React from 'react';

const GameRules = () => (
  <div className="description-wrapper">
    <div className="description">
      <p>
        The objective of Chain Reaction is to take control of the board by
        elimintaing your opponents orbs.
      </p>
      <p>
        Player takes it in turn to place their orbs in a cell. Once a cell has
        reached critical mass the orbs explode into the surrounding cells adding
        an extra orb and claiming the cell for the player. A player may only
        place their orbs in a blank cell or a cell that contain orbs of their
        own colour. As soon as a player loose all their orbs they are out of the
        game.
      </p>
    </div>
  </div>
);

export default GameRules;
