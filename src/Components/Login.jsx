/* eslint-disable react/jsx-curly-newline */
/* eslint-disable operator-linebreak */
/* eslint-disable implicit-arrow-linebreak */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

const colors = ['red', 'green', 'blue', 'yellow', 'white', 'magenta', 'purple'];

const Login = (props) => {
  const history = useHistory();

  const [players, setPlayers] = useState([
    {
      title: 'Enter player 1 name',
      name: '',
      color: 'red',
      showColorPicker: false,
    },
    {
      title: 'Enter player 2 name',
      name: '',
      color: 'green',
      showColorPicker: false,
    },
  ]);

  useEffect(() => {
    props.setLoginStatus(true);
  }, []);

  const setupHandleChange = (data, type) => {
    const allPlayer = JSON.parse(JSON.stringify(players));
    allPlayer.forEach((player, index) => {
      if (index === data.playerIndex) {
        switch (type) {
          case 'changeName':
            allPlayer[index].name = data.name;
            break;
          case 'changeColor':
            allPlayer[index].color = colors[data.colorIndex];
            allPlayer[index].showColorPicker = false;
            break;
          case 'setColorPicker':
            allPlayer[index].showColorPicker = !allPlayer[index]
              .showColorPicker;
            break;
          default:
            break;
        }
      }
    });
    setPlayers(allPlayer);
  };

  const handleSubmit = () => {
    if (
      players[0].name &&
      players[1].name &&
      players[0].color !== players[1].color
    ) {
      const finalPayload = {
        player1: players[0].name,
        player2: players[1].name,
        player1Color: players[0].color,
        player2Color: players[1].color,
      };

      const location = {
        pathname: '/playground',
        state: { finalPayload },
      };
      history.push(location);
      props.setLoginStatus(true);
    }
  };

  const keyDownFunction = (e, data, type) => {
    if (e.which === 13) {
      if (type === 'setColorPicker') {
        setupHandleChange({ playerIndex: data.playerIndex }, 'setColorPicker');
      }
      if (type === 'changeColor') {
        setupHandleChange(
          { playerIndex: data.playerIndex, colorIndex: data.colorIndex },
          'changeColor',
        );
      }
    }
  };

  return (
    <div className="loginPage">
      <p className="title">Chain Reaction</p>
      <div className="loginBox">
        {players.map((player, playerIndex) => (
          <>
            <p>{player.title}</p>
            <input
              value={player.name}
              onChange={(e) =>
                setupHandleChange(
                  { name: e.target.value, playerIndex },
                  'changeName',
                )
              }
            />
            <span
              aria-label="change_player_colour"
              role="button"
              tabIndex={-1}
              onKeyPress={(e) => {
                keyDownFunction(e, { playerIndex }, 'setColorPicker');
              }}
              onClick={() =>
                setupHandleChange({ playerIndex }, 'setColorPicker')
              }
              style={{ backgroundColor: player.color }}
              className="active-color"
            />
            {player.showColorPicker ? (
              <div className="color-pick-box">
                {colors.map((color, colorIndex) => (
                  <span
                    aria-label="change_player_colour"
                    role="button"
                    onKeyPress={(e) => {
                      keyDownFunction(
                        e,
                        { playerIndex, colorIndex },
                        'changeColor',
                      );
                    }}
                    tabIndex={-1}
                    onClick={() => {
                      setupHandleChange(
                        { playerIndex, colorIndex },
                        'changeColor',
                      );
                    }}
                    style={{ backgroundColor: color }}
                    className="color-dot"
                  />
                ))}
              </div>
            ) : (
              ''
            )}
          </>
        ))}
        <button
          disabled={
            !players[0].name.trim() ||
            !players[1].name.trim() ||
            players[0].color === players[1].color
          }
          onClick={() => handleSubmit()}
          type="submit"
        >
          Start Game
        </button>
      </div>
    </div>
  );
};

Login.propTypes = {
  setLoginStatus: PropTypes.func.isRequired,
};

export default Login;
