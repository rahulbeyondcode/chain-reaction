import React, { useState, useEffect } from 'react';
import { NotificationContainer } from 'react-notifications';
import { Switch, Route, useHistory } from 'react-router-dom';

import PlayZone from './Components/PlayZone';
import './Styles/index.css';
import Login from './Components/Login';

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const history = useHistory();

  useEffect(() => {
    if (isLoggedIn) {
      history.push('/playground');
    } else {
      history.push('/login');
    }
  }, [isLoggedIn]);

  return (
    <div className="App">
      <Switch>
        <Route
          path="/login"
          render={() => (
            <Login setLoginStatus={(value) => setIsLoggedIn(value)} />
          )}
        />
        <Route path="/playground" component={PlayZone} />
      </Switch>
      <NotificationContainer />
    </div>
  );
}

export default App;
